
# JS Unit Testing task

This repo contains the project for unit testing task.

## Notes  

Used latest Node LTS version, and latests versions for mocha, c8, awesomemocha.

Eslint and it's script is available even if it's not part of the task requirements. Expect a couple of changes on numbers-validator.js file related to eslint fixes.

Husky was installed but not enabled because there were some errors that I couldn't fix on time.

### Changes to the provided number-validator.js file

Since using latest versions of nodejs and suggested libraries, I had to modify the number-validator.js file provided for the task.  
The changes I made were add export to the class declaration and comment the last line that exported the module. Those changes have comments in the file.