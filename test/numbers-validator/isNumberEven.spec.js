import {NumbersValidator} from '../../app/numbers_validator.js';
import {expect} from 'chai';

describe('isNumberEven function tests', () => {
  let validator;
  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('Should return true if passed number is even', () => {
    const validationResults = validator.isNumberEven(4);
    expect(validationResults).to.be.equal(true);
  });

  it('Should return false if passed number is not even', () => {
    const validationResults = validator.isNumberEven(5);
    expect(validationResults).to.be.equal(false);
  });

  it('Should throw an error if typeof element is not number', () => {
    expect(() => {
      validator.isNumberEven('kitten');
    }).to.throw('[kitten] is not of type "Number" it is of type "string"');
  });
});
