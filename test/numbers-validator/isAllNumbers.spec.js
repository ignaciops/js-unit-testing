import {NumbersValidator} from '../../app/numbers_validator.js';
import {expect} from 'chai';

describe('isAllNumbers function tests', () => {
  let validator;
  beforeEach(() => {
    validator = new NumbersValidator();
  });

  it('Should return true if all objects in the array are numbers', () => {
    const testArray = [2, 4, 5, 10];
    const validationResults = validator.isAllNumbers(testArray);
    expect(validationResults).to.be.equal(true);
  });

  it('Should return false if not all objects in the array are numbers', () => {
    const testArray = [2, true, 'hello', 5, 8];
    const validationResults = validator.isAllNumbers(testArray);
    expect(validationResults).to.be.equal(false);
  });

  it('Should throw an error if other than an array is passed', () => {
    expect(() => {
      validator.isAllNumbers('hello world');
    }).to.throw('[hello world] is not an array');
  });
});
