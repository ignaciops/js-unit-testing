import {NumbersValidator} from '../../app/numbers_validator.js';
import {expect} from 'chai';

describe('getEvenNumbersFromArray function tests', () => {
  let validator;
  beforeEach(() => {
    validator = new NumbersValidator;
  });

  afterEach(() => {
    validator = null;
  });

  it('Should return the even numbers from an array of numbers', () => {
    const testArray = [1, 2, 3, 4, 5, 20, 438, 7];
    const expectedArray = [2, 4, 20, 438];
    const validationResults = validator.getEvenNumbersFromArray(testArray);
    expect(validationResults).to.deep.equal(expectedArray);
  });

  it('Should throw an error if array contains not only numbers', () => {
    const testArray = [2, 4, 'cat', true, 5];
    expect(() => {
      validator.getEvenNumbersFromArray(testArray);
    }).to.throw('[2,4,cat,true,5] is not an array of "Numbers"');
  });
});
