import {NumbersValidator} from '../../app/numbers_validator.js';
import {expect} from 'chai';

describe('isInteger function tests', () => {
  let validator;
  beforeEach(() => {
    validator = new NumbersValidator();
  });

  afterEach(() => {
    validator = null;
  });

  it('Shuold return true if a number is passed', () => {
    const validationResults = validator.isInteger(5);
    expect(validationResults).to.be.equal(true);
  });

  it('Should return false if number is not integer', () => {
    const validationResults = validator.isInteger(3.1416);
    expect(validationResults).to.be.equal(false);
  });

  it('Should throw an error if passed value is not a number', () => {
    expect(() => {
      validator.isInteger('cat');
    }).to.throw('[cat] is not a number');
  });
});
